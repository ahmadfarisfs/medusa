# This import registers the 3D projection, but is otherwise unused.
from mpl_toolkits.mplot3d import Axes3D  # noqa: F401 unused import
import matplotlib.pyplot as plt
import numpy as np
from matplotlib import cm
from bfs import commander
from device_comm import dummy_mech

reader = dummy_mech.DummyZSensor()
motor = dummy_mech.DummyMotor()
comm = commander.BfsCommander(motor,reader)

comm.set_scan_area(100)
comm.set_scan_resolution(2)
comm.set_delay_between_scan(0)
comm.set_center_impact_location(0,100,100)
comm.set_center_impact_location(1,0,0)
xs,ys,zs = comm.execute_scan(1)

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.plot_surface(xs,ys,zs,linewidth=0.2, antialiased=True)
plt.show()

# test dummy scan.ok. V2