from enum import Enum
from transitions import Machine
import logging

class StatusEnum(Enum):
    IDLE=0
    RDY_TEST_ONE = 1
    TESTING_ONE = 2
    RDY_TEST_TWO = 3
    TESTING_TWO = 4
    WAITING_EXIT = 5

class MasterSequencer(object):
    states = ['IDLE','READY_TEST_ONE','TESTING_ONE','READY_TEST_TWO','TESTING_TWO','WAITING_EXIT']

    def __init__(self, web_api, test_driver_one,test_driver_two,logger):
        self.web_api = web_api
        self.test_driver_one = test_driver_one
        self.test_driver_two = test_driver_two
        self.current_session_id = -1
        self.current_result_one = None
        self.current_result_two = None

        self.fsm  = Machine(model=self,states=MasterSequencer.states,initial='IDLE')
        print("init")
        self.fsm.add_transition('RFID_VALID','IDLE','READY_TEST_ONE')
        self.fsm.add_transition('START_TEST_ONE','READY_TEST_ONE','TESTING_ONE')
        self.fsm.add_transition('FINISH_TEST_ONE','TESTING_ONE','READY_TEST_TWO')
        self.fsm.add_transition('START_TEST_TWO','READY_TEST_TWO','TESTING_TWO')
        self.fsm.add_transition('FINISH_TEST_TWO','TESTING_TWO','WAITING_EXIT')
        self.fsm.add_transition('RFID_VALID','WAITING_EXIT','IDLE')
        self.fsm.add_transition('CANCEL','*','IDLE')
        #self.fsm.on_enter_READY_TEST_ONE('self.ready_test_one')

    def accept_rfid(self,rfid_id):
        #check, ignore pylint error because of dynamic attributes 
        if self.test_driver_one.is_ready() and self.test_driver_two.is_ready():
            if (self.web_api.check_integrity(id)):
                self.RFID_VALID()
            else:
                self.CANCEL()
        else:
            print("device is not ready")

    def start_test(self, id):
        if id ==1 :
            self.START_TEST_ONE()
        elif id ==2:
            self.START_TEST_TWO()
        else:
            self.CANCEL()

    def cancel_test(self):
        self.CANCEL()

    def end_test(self,id):
        if id ==1 :
            self.FINISH_TEST_ONE()
        elif id ==2:
            self.FINISH_TEST_TWO()
        else:
            self.CANCEL()
    
    # ---------------AUTOGEN
    def on_enter_IDLE(self):
        print('idle')
        self.current_session_id = -1
        self.current_result_one = None
        self.current_result_two = None
        self.test_driver_one.disable()
        self.test_driver_two.disable()
    
    # --------------1
    def on_enter_READY_TEST_ONE(self):
        print("ready test one")
        self.test_driver_one.enable()
    
    def on_exit_TESTING_ONE(self):
        self.current_result_one = self.test_driver_one.get_result()
        self.test_driver_one.disable()

    def on_enter_TESTING_ONE(self):
        print("Enter Testing one")
    #------------------


    #----------------- 2
    def on_enter_READY_TEST_TWO(self):
        print("ready test two")
        self.test_driver_two.enable()
    
    def on_exit_TESTING_TWO(self):
        self.current_result_two = self.test_driver_two.get_result()
        self.test_driver_two.disable()

    def on_enter_TESTING_TWO(self):
        print("Enter Testing two")

    #--------------
    def on_exit_WAITING_EXIT(self):
        self.web_api.send_to_server(self.current_session_id,self.current_result_one,self.current_result_two)
        print("send to server, save to local")