from machine_seq import sequencer
from web_api import dummyAPI
from device import dummy_test_device
from device import rfid_device
from comm import rfid_driver

api = dummyAPI.DummyAPI()
driver_one = dummy_test_device.DummyTestDevice(1)
driver_two = dummy_test_device.DummyTestDevice(2)

rfid_protocol = rfid_driver.RFIDProtocol("/ttyUSB0")
rfid_device = rfid_device.RFIDDriver(rfid_protocol)


seq = sequencer.MasterSequencer(api,driver_one,driver_two,None)

rfid_device.set_detected_callback(seq.accept_rfid)