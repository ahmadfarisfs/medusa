import time
import numpy as np
import pandas as pd
from scipy.interpolate import griddata
# all units in mm
class BfsCommander:
    def __init__(self,motor_driver,z_reader_driver):
        self.motor_driver = motor_driver
        self.z_reader_driver = z_reader_driver
        self.id_location_dict = dict()
        self.scan_area = 10
        self.resolution = 1
        self.delay_scan=10

    def initialize(self):
        self.motor_driver.home()
    
    def set_center_impact_location(self,id,x,y):
        self.id_location_dict[id]=(x,y)
    
    def get_center_impact_location(self,id):
        return self.id_location_dict[id]
    
    def set_scan_area(self,offset):
        self.scan_area = offset
    
    def set_scan_resolution(self,resolution):
        self.resolution = resolution
    
    def _generate_scan_points(self,center_x,center_y):
        # generate snake pattern scanning
        arr = []
        reverse = False
        for x in range(int(self.scan_area/self.resolution)+1):
            if not reverse:
                for y in range(int(self.scan_area/self.resolution)+1):
                    arr.append((x*self.resolution+center_x-self.scan_area/2,y*self.resolution+center_y-self.scan_area/2))
            else:
                for y in reversed(range(int(self.scan_area/self.resolution)+1)):
                    arr.append((x*self.resolution+center_x-self.scan_area/2,y*self.resolution+center_y-self.scan_area/2))
            reverse = not reverse
        return arr
    
    def set_delay_between_scan(self,delay_ms):
        self.delay_scan = delay_ms

    def execute_scan(self,id):
        center_impact_coordinate = self.get_center_impact_location(id)
        scan_points = self._generate_scan_points(center_impact_coordinate[0],center_impact_coordinate[1])
        print("center: ")
        print(center_impact_coordinate)
        result = []
        xs=[]
        ys=[]
        zs=[]
        for point in scan_points:
            self.motor_driver.move_to(point[0],point[1])
            time.sleep(self.delay_scan/1000)
            height = self.z_reader_driver.read_height(point[0],point[1])
            result.append((point[0],point[1],height))
            xs.append(point[0])
            ys.append(point[1])
            zs.append(height)
            
        # data adjustment https://stackoverflow.com/questions/9170838/surface-plots-in-matplotlib
        xyz = {'x': xs, 'y': ys, 'z': zs}
        df = pd.DataFrame(xyz, index=range(len(xyz['x'])))
        # re-create the 2D-arrays
        x1 = np.linspace(df['x'].min(), df['x'].max(), len(df['x'].unique()))
        y1 = np.linspace(df['y'].min(), df['y'].max(), len(df['y'].unique()))
        x2, y2 = np.meshgrid(x1, y1)
        z2 = griddata((df['x'], df['y']), df['z'], (x2, y2), method='cubic')
        return x2,y2,z2