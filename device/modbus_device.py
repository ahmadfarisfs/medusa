from threading import Thread
import time

class TestDevice(object):
    ENABLE_ADDRESS = 0x00
    DISABLE_ADDRESS = 0x00
    
    ENABLE_CMD = 0xFF
    DISABLE_CMD = 0x00

    RESULT_ADDRESS = 0x06
    DATA_READY_ADDRESS = 0x10
    START_ADDRESS = 0x40
    CANCEL_ADDRESS = 0x50

    def __init__(self,protocol):
        self.protocol = protocol
        self.start_cb = None
        self.cancel_cb = None
        self.event_gen_thread = Thread(target=TestDevice.loop_thread)
        self.data_ready_cb = None
    
    def is_ready(self):
        return self.protocol.read_coil(TestDevice.ENABLE_ADDRESS)

    def set_start_test_callback(self, callback):
        self.start_cb = callback
    
    def set_finish_test_callback(self,callback):
        self.data_ready_cb = callback

    def set_cancel_test_callback(self,callback):
        self.cancel_cb = callback

    def loop_thread(self):
        while True:
            start =  self.protocol.read_coil(TestDevice.START_ADDRESS)
            cancel = self.protocol.read_coil(TestDevice.CANCEL_ADDRESS)
            data_ready = self.protocol.read_coil(TestDevice.DATA_READY_ADDRESS)
            if start:
                self.start_cb()
                self.protocol.write_coil(TestDevice.START_ADDRESS,False)
            if data_ready:
                self.data_ready_cb(self.get_result())                
                self.protocol.write_coil(TestDevice.DATA_READY_ADDRESS,False)
            if cancel:
                self.cancel_cb()
                self.protocol.write_coil(TestDevice.CANCEL_ADDRESS, False)
            time.sleep(0.05)
    
    def enable(self):
        self.protocol.write_coil(TestDevice.ENABLE_ADDRESS,TestDevice.ENABLE_CMD,True)
    
    def disable(self):
        self.protocol.write_coil(TestDevice.DISABLE_ADDRESS,TestDevice.DISABLE_CMD,False)

    def get_result(self):
        return self.protocol.read_register(TestDevice.RESULT_ADDRESS)
