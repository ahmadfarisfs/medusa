from threading import Thread
import time

class RFIDDriver(object):
    def __init__(self,protocol):
        self.protocol = protocol
        self.detected_cb = None
        self.event_gen_thread = Thread(target=RFIDDriver.loop_thread)

    def set_detected_callback(self, callback):
        self.detected_cb = callback

    def loop_thread(self):
        while True:
            rfid_id =  self.protocol.read_id()
            if rfid_id:
                self.detected_cb(rfid_id)
            time.sleep(0.05)