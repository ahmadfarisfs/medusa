class BFSDevice(object):
    def __init__(self,bfs_commander):
        self.commander = bfs_commander
        self.ready = False
        self.start_cb =None
        self.data_ready_cb = None
        self.cancel_cb = None

    def enable(self):
        self.ready = True
   
    def disable(self):
        self.ready=False
    
    def is_ready(self):
        return self.ready

    def set_start_test_callback(self, callback):
        self.start_cb = callback
    
    def set_finish_test_callback(self,callback):
        self.data_ready_cb = callback

    def set_cancel_test_callback(self,callback):
        self.cancel_cb = callback
    
    def get_result(self):
        return
        