import math
class DummyTestDevice:
    def __init__(self,id):
        self.id = id
        self.ready = False

    def is_ready(self):
        return True
    
    def enable(self):
        self.ready = True

    def disable(self):
        self.ready = False
    
    def get_result(self):
        return None