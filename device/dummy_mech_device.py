import math
class DummyMotor:
    def move_to(self,x,y):
        print("Moved to "+str(x)+ ", "+str(y))

class DummyZSensor:
    def read_height(self,x,y):
        return -2*math.exp(-.01*(x**2+y**2))